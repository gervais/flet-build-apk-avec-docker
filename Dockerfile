# Dockerfile to use easily `flet run --web` or `flet build apk` commands

FROM ubuntu:latest
### better with an image like https://github.com/growerp/flutter-sdk-image ??

# Update the system and install dependencies
RUN apt-get update -y && \
    apt-get install -y python3 pipx curl unzip openjdk-21-jre git clang cmake \
                       ninja-build pkg-config libgtk-3-dev android-sdk xz-utils \
                       libgstreamer-plugins-base* libmpv* mpv && \
    ln -s /usr/lib/x86_64-linux-gnu/libmpv.so /usr/lib/x86_64-linux-gnu/libmpv.so.1
### See https://github.com/flet-dev/flet/issues/2823 for previous line
                      

# Install pip dependencies 
### ERROR "externally-managed-environment" with pip instead of pipx
RUN pipx install flet

### flet command still unrecocgnized, even with this command ?! (**)
RUN pipx ensurepath  

# Install Flutter
WORKDIR /development
RUN curl -O "https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.22.2-stable.tar.xz"  && \
    tar -xf "flutter_linux_3.22.2-stable.tar.xz" && \
    rm -rf "flutter_linux_3.22.2-stable.tar.xz" 

# Install SDKmanager
WORKDIR /development
RUN curl -o commandlinetools-linux-11076708_latest.zip -O https://dl.google.com/android/repository/commandlinetools-linux-11076708_latest.zip?hl=tr && \
    unzip commandlinetools-linux-11076708_latest.zip && \
    rm -rf commandlinetools-linux-11076708_latest.zip && \
    mkdir latest && \
    mv cmdline-tools/* latest && \
    mv latest/ cmdline-tools/

# Set paths
ENV PATH $PATH:/path/to/nvm/bin:/path/to/rvm/bin
ENV PATH $PATH:/development/flutter/bin
ENV PATH $PATH:/development/cmdline-tools/latest/bin
ENV PATH $PATH:/root/.local/share/pipx/venvs/flet/bin/

## Should be useless, see above (**)
RUN ln -s /root/.local/share/pipx/venvs/flet/bin/flet /usr/local/bin/flet

# Accept licenses and install dependencies via sdkmanager
RUN git config --global --add safe.directory /development/flutter && \
    yes | sdkmanager --sdk_root=/usr/lib/android-sdk/ --install "ndk;26.1.10909125" "cmdline-tools;latest" && \
    yes | sdkmanager --licenses && \
    yes | flutter doctor --android-licenses && \
    flutter --disable-analytics
    
#RUN flutter upgrade

