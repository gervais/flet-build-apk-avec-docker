# flet build apk avec Docker

* L'idée est d'utiliser une Docker pour disposer plus simplement de tous les 
outils nécessaires à la compilation en une appli Android `.apk` du code 
Python écrit pour `flet`.

* À noter que l'on peut facilement exécuter l'appli en mode web par exemple, 
mais qu'il y a bien plus de choses à installer pour exporter en `.apk`.

> _**Note**_ : il s'agit pour moi d'une activité de découverte qui ne sera 
pas forcément suivie. Je ne suis spécialiste ni de Docker, ni de flet ou
de flutter.

## À disposition

* Le `Dockerfile` à utiliser pour créer l'image. Il faut donc avoir 
  installé Docker, bien entendu.

* Un pense-bête pour avoir en tête les principales étapes et commandes 
  associées
  
# Ressources

<https://flet.dev/docs/>

<https://www.cheatsheet.fr/2024/06/05/creer-une-application-android-avec-flet/>

<https://docs.docker.com/storage/volumes/>
