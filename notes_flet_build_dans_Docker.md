# Préparation, depuis l'hôte
* _Pas indispensable si l'on indique un chemin 
  absolu pour le volume, dans un dossier local, avec `$(pwd)/`_ :    
  `docker volume create apk`

* Construction de l'image Docker
  `docker image build -t flet_app_builder .` (_ne pas oublier le `.` final_)

# Création ou modification du projet d'appli flet

## Lancement de la machine Docker

* Pour récupérer l'appli dans le dossier local `apk/`,    
  `docker run -it -v $(pwd)/apk:/apk -p 80:8000  flet_app_builder` 

* ou sinon    
  `docker run -it -v apk:/development -p 80:8000  flet_app_builder` pour 
  utiliser un volume classique.

* ou encore, sans volume mais en copiant l'artefact ensuite depuis l'image (on
  peut copier aussi le projet ou autre depuis l'hôte vers l'image, voir 
  ci-dessous).

## Création du projet sur la machine Docker

* Pour créer un projet vierge ou d'après un modèle :    
  `flet create --template counter test`    
  ou sinon, pour créer un projet dans le dossier courant, `flet create .`

* On peut passer par un volume pour récupérer du code préparé avant...
  (le contenu d'un volume classique est situé, hors Docker, sur 
  `/var/lib/docker/volumes/apk/_data/`, ou sinon à 
  l'emplacement où on l'a indiqué avec `$(pwd)/...`)

## Utilisation comme appi web, pour tester rapidement

Sur la machine Docker, `flet run --web --port 8000 test` permet de valider 
le bon fonctionnement (voir sur `localhost` dans le navigateur de l'hôte).
Si on ne l'utilise pas, les parmaètres de port `-p 80:8000` plus haut ne
sont pas utiles.


# Compilation et installation d'une appli Android `.apk`

* Sur la machine Docker, `flet build --no-android-splash -o apk/ apk test` (long)

* On peut récupérer l'apk dans le dossier du volume. Alternative : ne pas 
  utiliser forcément de volume et copier ensuite sur l'hôte le résultat, avec
  `cp ref_image:/chemin/sur/image/app-release.apk chemin/sur/hote/` (on peut
  intervertir les paramètres pour copier dans l'autre sens, vers l'image).

* Puis, sur le téléphone à connecter à l'hôte en USB, activer le mode débogage, 
  avec : Paramètres / Système / Paramètres avancés / Options pour les 
  développeurs / Débogage USB (à activer).

* Enfin, depuis l'hôte, dans le répertoire de l'apk, pour installer l'appli :    
  `adb install  app-release.apk`

# Références

<https://flet.dev/docs/>
<https://www.cheatsheet.fr/2024/06/05/creer-une-application-android-avec-flet/>
<https://docs.docker.com/storage/volumes/>
